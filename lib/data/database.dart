import 'dart:async';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

final tableName = 'User';

class DatabaseProvider {
  static final DatabaseProvider dbProvider = DatabaseProvider();
  Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await createDatabase();
    return _database;
  }

  createDatabase() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, "user_db.db");
    var database = await openDatabase(path, version: 1, onCreate: initDB);
    return database;
  }

  void initDB(Database database, int version) async {
    String sql = "CREATE TABLE $tableName ( "
        "id INTEGER PRIMARY KEY,"
        "email TEXT,"
        "first_name TEXT,"
        "last_name TEXT,"
        "avatar TEXT,"
        "favorite INTEGER )";
    await database.execute(sql);
  }
}
