import 'package:flutter_users_view/models/user_model.dart';

import 'user_dao.dart';

class UserRepository {
  final userDao = UserDao();

  Future<List<User>> getAllFavoriteUsers() => userDao.getAllFavoriteUsers();
  Future insertUser(User user) => userDao.insertUser(user);
  Future deleteUserById(User user) => userDao.deleteUser(user);
  Future deleteAllUsers() => userDao.deleteAllUsers();
  Future<bool> getFavoriteUser(User user) => userDao.getFavoriteUser(user);
}
