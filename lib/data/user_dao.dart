import 'dart:async';

import 'database.dart';
import '../models/user_model.dart';

class UserDao {
  final dbProvider = DatabaseProvider.dbProvider;

  //Добавление нового пользователя в БД
  Future<int> insertUser(User user) async {
    final db = await dbProvider.database;
    Future<int> result;
    var findedUser = await db.query(
      tableName,
      where: 'id = ?',
      whereArgs: [user.id],
    );
    if (findedUser.isEmpty) {
      user.favorite = true;
      result = db.insert(tableName, user.toJson());
    }
    return result;
  }

  //Получить всех избранных пользователей
  Future<List<User>> getAllFavoriteUsers() async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result;
    result = await db.query(tableName);

    List<User> users = result.isNotEmpty
        ? result.map((item) => User.fromJson(item)).toList()
        : [];

    return users;
  }

  //Проверка существования пользователя в БД
  Future<bool> getFavoriteUser(User user) async {
    final db = await dbProvider.database;
    var result = await db.query(
      tableName,
      where: 'id = ?',
      whereArgs: [user.id],
    );
    return result.isNotEmpty;
  }

  //Удаление пользователя из БД
  Future<int> deleteUser(User user) async {
    final db = await dbProvider.database;
    var result = await db.delete(
      tableName,
      where: 'id = ?',
      whereArgs: [user.id],
    );
    user.favorite = false;
    return result;
  }

  //Удаление всей таблицы пользователи
  Future deleteAllUsers() async {
    final db = await dbProvider.database;
    var result = await db.delete(tableName);
    return result;
  }
}
