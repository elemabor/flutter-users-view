import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;

import './bloc/user_bloc/user.dart';
import './screens/users_list.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final UserBloc _userBloc = UserBloc(httpClient: http.Client())
    ..add(UserFetched());

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => _userBloc,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter users view',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: UsersList(),
      ),
    );
  }
}
