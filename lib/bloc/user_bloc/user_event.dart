import 'package:equatable/equatable.dart';

import '../../models/user_model.dart';

abstract class UserEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class UserFetched extends UserEvent {}

class UserGetFavorite extends UserEvent {}

class UserAddToFavorite extends UserEvent {
  final User user;

  UserAddToFavorite(this.user);

  @override
  List<Object> get props => [user];
}

class UserRemoveFromFavorite extends UserEvent {
  final User user;

  UserRemoveFromFavorite(this.user);

  @override
  List<Object> get props => [user];
}

class UserToogleFavorite extends UserEvent {}

class UserSelectedForDetail extends UserEvent {
  final User user;

  UserSelectedForDetail(this.user);

  @override
  List<Object> get props => [user];
}
