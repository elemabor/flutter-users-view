import 'dart:convert';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission_handler/permission_handler.dart';

import 'user_event.dart';
import 'user_state.dart';
import '../../data/user_repository.dart';
import '../../models/response_model.dart';
import '../../models/user_model.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  final http.Client httpClient;
  final userRepo = UserRepository();
  static const int _usersPerPage = 6;

  UserBloc({@required this.httpClient}) : super(const UserState());

  @override
  Stream<UserState> mapEventToState(UserEvent event) async* {
    var status = await Permission.storage.request().isGranted;
    if (status) {
      if (event is UserFetched) {
        yield await _mapUserFetchedToState(state);
      } else if (event is UserAddToFavorite) {
        yield await _mapUserAddToFavorite(event);
      } else if (event is UserRemoveFromFavorite) {
        yield await _mapUserRemoveFromFavorite(event);
      } else if (event is UserToogleFavorite) {
        yield await _mapToogleFavoriteUser(state);
      } else if (event is UserSelectedForDetail) {
        yield await _selectUserForDetail(event);
      }
    } else {
      yield state.copyWith(status: UserStatus.error);
    }
  }

  Future<UserState> _mapUserFetchedToState(UserState state) async {
    if (state.hasReachedMax) {
      return state;
    }
    try {
      if (state.status == UserStatus.initial ||
          state.status == UserStatus.favorite) {
        var users = await _fetchUsers(1);
        var result = await _updateFavorite(users);
        return state.copyWith(
          status: UserStatus.loaded,
          users: result,
          hasRechedMax: false,
        );
      }
      final users = await _fetchUsers(state.users.length ~/ _usersPerPage + 1);
      var result = await _updateFavorite(users);
      return users.isEmpty
          ? state.copyWith(hasRechedMax: true)
          : state.copyWith(
              status: UserStatus.loaded,
              users: List.of(state.users)..addAll(result),
              hasRechedMax: false,
            );
    } catch (_) {
      return state.copyWith(status: UserStatus.error);
    }
  }

  Future<List<User>> _fetchUsers(int pageNumber) async {
    final response =
        await httpClient.get('https://reqres.in/api/users?page=$pageNumber');
    if (response.statusCode == 200) {
      final data = json.decode(response.body);
      List<User> users = Response.fromJson(data).users;
      return users;
    } else {
      throw Exception("Can`t fetching users");
    }
  }

  Future<UserState> _mapUserAddToFavorite(UserAddToFavorite event) async {
    try {
      await userRepo.insertUser(event.user);
      return state;
    } catch (_) {
      throw Exception("Can`t add user");
    }
  }

  Future<UserState> _mapUserRemoveFromFavorite(
      UserRemoveFromFavorite event) async {
    try {
      await userRepo.deleteUserById(event.user);
      return state;
    } catch (_) {
      throw Exception("Can`t remove user");
    }
  }

  Future<UserState> _mapUserGetFavorite() async {
    try {
      var allFavoriteUsers = await userRepo.getAllFavoriteUsers();
      return allFavoriteUsers.isNotEmpty
          ? state.copyWith(
              status: UserStatus.favorite,
              users: allFavoriteUsers,
              hasRechedMax: false,
            )
          : state.copyWith(
              status: UserStatus.favorite,
              users: allFavoriteUsers,
              hasRechedMax: false,
            );
    } catch (_) {
      return state.copyWith(status: UserStatus.error);
    }
  }

  Future<UserState> _mapToogleFavoriteUser(UserState state) async {
    try {
      UserState newState;
      newState = state.status == UserStatus.favorite
          ? await _mapUserFetchedToState(state)
          : await _mapUserGetFavorite();
      return newState;
    } catch (_) {
      return state.copyWith(status: UserStatus.error);
    }
  }

  Future<UserState> _selectUserForDetail(UserSelectedForDetail event) async {
    return state.copyWith(selectedUser: event.user);
  }

  Future<List<User>> _updateFavorite(List<User> list) async {
    List<User> result = List<User>();
    for (var user in list) {
      if (await userRepo.getFavoriteUser(user)) {
        user.favorite = true;
      }
      result.add(user);
    }

    return result;
  }
}
