import 'package:equatable/equatable.dart';
import 'package:flutter_users_view/models/user_model.dart';

enum UserStatus { initial, loaded, error, favorite }

class UserState extends Equatable {
  final UserStatus status;
  final List<User> users;
  final bool hasReachedMax;
  final User selectedUser;

  const UserState({
    this.status = UserStatus.initial,
    this.users = const <User>[],
    this.hasReachedMax = false,
    this.selectedUser,
  });

  UserState copyWith({
    UserStatus status,
    List<User> users,
    bool hasRechedMax,
    User selectedUser,
  }) {
    return UserState(
      status: status ?? this.status,
      users: users ?? this.users,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      selectedUser: selectedUser ?? this.selectedUser,
    );
  }

  @override
  List<Object> get props => [status, users, hasReachedMax, selectedUser];
}
