class User {
  int id;
  String email;
  String firstName;
  String lastName;
  String avatar;
  bool favorite;

  User({
    this.id,
    this.email,
    this.firstName,
    this.lastName,
    this.avatar,
    this.favorite = false,
  });

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    avatar = json['avatar'];
    favorite = json['favorite'] == null || json['favorite'] == 0 ? false : true;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> user = new Map<String, dynamic>();
    user['id'] = this.id;
    user['email'] = this.email;
    user['first_name'] = this.firstName;
    user['last_name'] = this.lastName;
    user['avatar'] = this.avatar;
    user['favorite'] = this.favorite == true ? 1 : 0;
    return user;
  }
}
