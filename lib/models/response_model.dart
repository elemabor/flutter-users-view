import 'user_model.dart';

class Response {
  int page;
  int perPage;
  int total;
  int totalPages;
  List<User> users;

  Response({this.page, this.perPage, this.total, this.totalPages, this.users});

  Response.fromJson(Map<String, dynamic> json) {
    page = json['page'];
    perPage = json['per_page'];
    total = json['total'];
    totalPages = json['total_pages'];
    if (json['data'] != null) {
      users = new List<User>();
      json['data'].forEach((v) {
        users.add(new User.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['page'] = this.page;
    data['per_page'] = this.perPage;
    data['total'] = this.total;
    data['total_pages'] = this.totalPages;
    if (this.users != null) {
      data['data'] = this.users.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
