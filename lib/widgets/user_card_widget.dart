import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/user_bloc/user.dart';

import '../models/user_model.dart';

class UserCardWidget extends StatefulWidget {
  final User user;

  const UserCardWidget({Key key, this.user}) : super(key: key);

  @override
  _UserCardWidgetState createState() => _UserCardWidgetState();
}

class _UserCardWidgetState extends State<UserCardWidget> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Colors.black87),
            ),
            height: 120,
            width: MediaQuery.of(context).size.width * 0.9,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  child: CircleAvatar(
                    radius: 30,
                    backgroundImage: NetworkImage(widget.user.avatar),
                    backgroundColor: Colors.transparent,
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "${widget.user.firstName} ${widget.user.lastName}",
                      style: TextStyle(fontSize: 24),
                    ),
                    Text(
                      widget.user.email,
                      style: TextStyle(fontSize: 20),
                    )
                  ],
                ),
                IconButton(
                  icon: Icon(
                    Icons.star,
                    size: 32,
                    color:
                        widget.user.favorite ? Colors.black : Colors.grey[400],
                  ),
                  onPressed: () {
                    widget.user.favorite
                        ? BlocProvider.of<UserBloc>(context)
                            .add(UserRemoveFromFavorite(widget.user))
                        : BlocProvider.of<UserBloc>(context)
                            .add(UserAddToFavorite(widget.user));
                    setState(() {
                      widget.user.favorite = !widget.user.favorite;
                    });
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
