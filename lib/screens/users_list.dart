import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/user_bloc/user.dart';

import '../widgets/bottom_loader_widget.dart';
import '../widgets/user_card_widget.dart';
import 'detail_screen.dart';

class UsersList extends StatefulWidget {
  @override
  _UsersListState createState() => _UsersListState();
}

class _UsersListState extends State<UsersList> {
  final _scrollController = ScrollController();
  String _appBarTitle = "Все пользователи";

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey,
        title: Text(_appBarTitle),
        toolbarHeight: 50,
        actions: [
          IconButton(
            icon: Icon(
              Icons.check,
              color: Colors.black,
            ),
            onPressed: () {
              BlocProvider.of<UserBloc>(context).add(UserToogleFavorite());
              setState(() {
                _appBarTitle =
                    BlocProvider.of<UserBloc>(context).state.status ==
                            UserStatus.favorite
                        ? "Все пользователи"
                        : "Избранные пользователи";
              });
            },
          )
        ],
      ),
      body: BlocBuilder<UserBloc, UserState>(
        builder: (context, state) {
          switch (state.status) {
            case UserStatus.error:
              return Center(
                child: Text("Ошибка получения пользователей"),
              );
            case UserStatus.loaded:
              if (state.users.isEmpty) {
                return Center(
                  child: Text("Пользователи отсутствуют"),
                );
              }
              return ListView.builder(
                itemBuilder: (context, index) {
                  return index >= state.users.length
                      ? BottomLoaderWidget()
                      : GestureDetector(
                          child: UserCardWidget(
                            user: state.users[index],
                          ),
                          onTap: () {
                            BlocProvider.of<UserBloc>(context)
                                .add(UserSelectedForDetail(state.users[index]));
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => DetailUserPage(),
                              ),
                            ).then((value) {
                              setState(() {});
                            });
                          },
                        );
                },
                itemCount: state.users.length,
                controller: _scrollController,
              );
            case UserStatus.favorite:
              if (state.users.isEmpty) {
                return Center(
                  child: Text("Избранные пользователи отсутствуют"),
                );
              }
              return ListView.builder(
                itemBuilder: (context, index) => GestureDetector(
                  onTap: () {
                    BlocProvider.of<UserBloc>(context)
                        .add(UserSelectedForDetail(state.users[index]));
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DetailUserPage(),
                      ),
                    ).then((value) {
                      setState(() {});
                    });
                  },
                  child: UserCardWidget(
                    user: state.users[index],
                  ),
                ),
                itemCount: state.users.length,
                controller: _scrollController,
              );
            default:
              return Center(
                child: CircularProgressIndicator(),
              );
          }
        },
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_isBottom &&
        BlocProvider.of<UserBloc>(context).state.status !=
            UserStatus.favorite) {
      BlocProvider.of<UserBloc>(context).add(UserFetched());
    }
  }

  bool get _isBottom {
    if (!_scrollController.hasClients) return false;
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.offset;
    return currentScroll >= (maxScroll * 0.9);
  }
}
