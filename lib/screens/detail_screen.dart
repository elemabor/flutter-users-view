import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/user_bloc/user.dart';

class DetailUserPage extends StatefulWidget {
  @override
  _DetailUserPageState createState() => _DetailUserPageState();
}

class _DetailUserPageState extends State<DetailUserPage> {
  @override
  Widget build(BuildContext context) {
    var user = BlocProvider.of<UserBloc>(context).state.selectedUser;
    var buttonText =
        user.favorite ? "Удалить из избранного" : "Добавить в избранное";
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey,
      ),
      body: BlocBuilder<UserBloc, UserState>(
        builder: (context, state) => Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Center(
              child: CircleAvatar(
                radius: 100,
                backgroundImage: NetworkImage(user.avatar),
                backgroundColor: Colors.transparent,
              ),
            ),
            SizedBox(height: 20),
            Text(
              user.firstName,
              style: TextStyle(fontSize: 24),
            ),
            SizedBox(height: 10),
            Text(
              user.lastName,
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 30),
            Text(
              user.email,
              style: TextStyle(fontSize: 24, fontStyle: FontStyle.italic),
            ),
            SizedBox(height: 30),
            GestureDetector(
              onTap: () {
                user.favorite
                    ? BlocProvider.of<UserBloc>(context)
                        .add(UserRemoveFromFavorite(user))
                    : BlocProvider.of<UserBloc>(context)
                        .add(UserAddToFavorite(user));
                setState(() {
                  buttonText = BlocProvider.of<UserBloc>(context)
                          .state
                          .selectedUser
                          .favorite
                      ? "Удалить из избранного"
                      : "Добавить в избранное";
                });
              },
              child: Container(
                padding: EdgeInsets.all(8),
                child: Text(
                  buttonText,
                  style: TextStyle(color: Colors.black, fontSize: 24),
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  border: Border.all(),
                  color: Colors.grey,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
