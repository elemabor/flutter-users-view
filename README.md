# Тестовое задание на должность мобильного разработчика Flutter

## Задание

Необходимо реализовать приложение для показа списка пользователей. Данные получать используя [данный](https://reqres.in) сервис.

## Необходимый функционал приложения

В мобильном приложении необходимо создать 2 экрана. 

Первый экран: отображает список пользователей, из ранее упомянутого [сервиса](https://reqres.in), для отображения данных использовать постраничную разбивку, по 6-8 пользователей, для управления постраничным отображением использовать `"ленивую подгрузку"`, либо реализовать с помощью кнопки `"Показать еще"`. Сделать возможность отметить понравившегося пользователя (добавить в избранное). Сделать фильтр отображающий только избранных пользователей.

Второй экран: детальная информация пользователя. Все данные полученные о пользователе с возможность добавить в избранное.

## Разработка приложения

Для разработки данного приложения использовались следующие библиотеки:
```
  sqflite: ^1.3.2+1
  flutter_bloc: ^6.0.6
  http: ^0.12.2
  equatable: ^1.2.5
  rxdart: ^0.24.1
  permission_handler: ^5.0.1+1
```
### Ключевые библиотеки:
`sqflite` - данная библиотека позволяет реализовать локальную базу данных. В данной базе данных будут храниться пользователи после добавления в избранное. Также, данная библиотека позволяет хранить полученную информацию даже после перезапуска приложения.

`flutter_bloc` - данная библиотека позволяет проектировать приложение с помощью паттерна BLoC. Суть данного паттерна заключается в том, что все входы и выхода - это потоки. Выбор данного паттерна обусловлен тем, что с его помощью можно легко убрать логику из визуальной части интерфейса приложения.

`http` - библиотека для использования сетевых запросов. В данном приложении используется для получения списка пользователей.

`permission_handler` - данная библиотека предназначена для запросов доступа к отдельным элементам устройства. В данной реализации используется для запроса разрешения на запись во внутреннюю память (необходимость для корректной работы библиотеки sqflite).

### Разработанный интерфейс приложения

После запуска приложения пользователя встречает главный экран с фильтром показа всех пользователей. Для выбора только избранных пользователей необходимо нажать на правую верхнюю кнопку в виде ✔️. 

Для добавления пользователя в избранное необходимо нажать на кнопку в виде ⭐.

<p align="center">
  <b>Главный экран приложения с выбором всех пользователей</b>
</p>
<p align="center">
  <img width=400 src="/images/all_users.png">
</p>

<p align="center">
  <b>Главный экран приложения с выбором избранных пользователей</b>
</p>
<p align="center">
  <img width=400 src="/images/favorite_users.png">
</p>

<p align="center">
  <b>Второй экран приложения с подробной информацией (пользователь уже в избранном)</b>
</p>
<p align="center">
  <img width=400 src="/images/detail_favorite.png">
</p>

<p align="center">
  <b>Второй экран приложения с подробной информацией (пользователя можно добавить в изрбранное)</b>
</p>
<p align="center">
  <img width=400 src="/images/detail_not_favorite.png">
</p>

## Заключение

В ходе выполнения данного тестового задания было разработано мобильное приложение на Flutter `v1.22.3`. 

Пользователи загружались с указанного в задании [сервиса](https://reqres.in). Один запрос возвращал 6 пользователей. Для управления постраничным отображением использовалась `"ленивая подгрузка"`, т.е. когда пользователь приложения доходит до конца списка новые данные начинают подгружаться.

Для управления состоянием приложения использовалась библиотека `flutter_bloc`, хранение данных на устройстве пользователя мобильного приложения реализовано с помощью библиотеки `sqflite`.